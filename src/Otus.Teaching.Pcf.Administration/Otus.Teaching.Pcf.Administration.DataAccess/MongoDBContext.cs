﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.DataAccess.DatabaseSettings;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoDBContext : IMongoDBContext
    {
        private IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }
        private readonly AdministrationDatabaseSettings databaseSettings;

        private string GetUrl
        {
            get
            {
                return $@"mongodb://{databaseSettings.Login}:{databaseSettings.Password}@{databaseSettings.ConnectionString}";
            }
        }


        public MongoDBContext(IConfiguration configurationData)
        {
            databaseSettings = configurationData.GetSection("AdministrationDatabaseSettings").Get<AdministrationDatabaseSettings>();
            _mongoClient = new MongoClient(GetUrl);
            _db = _mongoClient.GetDatabase(databaseSettings.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }

    public interface IMongoDBContext
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }
}
