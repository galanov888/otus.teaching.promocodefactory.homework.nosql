﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Employee> Employees => new List<Employee>()
        {
            
            new Employee()
            {
                Id = ("5fa2ef52c038f899af9c187b"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = ("5fa2ef52c038f899af9c187c"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };
            

        public static List<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = ("5fa2c54a8001144baa53cad6"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = ("5fa2c54a8001144baa53cad7"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
    }
}