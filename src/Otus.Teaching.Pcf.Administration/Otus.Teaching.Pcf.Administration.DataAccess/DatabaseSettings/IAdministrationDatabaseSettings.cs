﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.DatabaseSettings
{
    public interface IAdministrationDatabaseSettings
    {
        string EmployeesCollectionName { get; set; }
        string RolesCollectionName { get; set; }

        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
