﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.DatabaseSettings
{
    public class AdministrationDatabaseSettings : IAdministrationDatabaseSettings
    {
        public string EmployeesCollectionName { get; set; }
        public string RolesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
