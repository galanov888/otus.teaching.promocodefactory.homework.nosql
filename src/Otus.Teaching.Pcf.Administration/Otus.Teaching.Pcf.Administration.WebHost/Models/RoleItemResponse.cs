﻿using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class RoleItemResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}